<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('matricule')->unique();
            $table->string('password');
            $table->string('lastName')->nullable();
            $table->string('firstName')->nullable();
            $table->string('sex')->nullable();
            $table->string('nationality')->nullable();
            $table->string('phoneNumber',15)->nullable();
            $table->string('lastAddress')->nullable();
            $table->string('testStatus')->nullable();
            $table->string('profile')->default('SUSPECT');
            //$table->string('email')->unique();
            //$table->timestamp('email_verified_at')->nullable();
            //$table->rememberToken();
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
