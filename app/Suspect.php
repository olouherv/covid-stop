<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suspect extends Model
{
    //
    public $timestamps = false;

    protected $fillable = [
        'name','type','address',
    ];

    public function informations(){
        return $this->hasMany('App\Information');
    }
}
