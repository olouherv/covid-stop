<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Information extends Model
{
    //
    public $timestamps = false;

    protected $fillable = [
        'name',
    ];

    public function place(){
        return $this->belongsTo('App\Place');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

}
