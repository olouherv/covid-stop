<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">


    <!-- CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">

    <!-------------------------->
    <title>@yield('title') - COVIDSTOP</title>
</head>
<body>

<header>
    <nav class="navbar" style="background: #1abc9c">
        <div class="container" style="color: white;">
            <div> Numéro d'urgence : +229 XX XX XX XX</div>
            <div> Suivez nous : <a href=""><img src="{{ asset('assets/img/icons/twitter.png') }}" alt="Twitter"></a></div>
        </div>

    </nav>
</header>

<main style="background-image: url('{{ asset('assets/img/bg.jpg') }}'); background-repeat: no-repeat; background-size: cover;">

    @yield('content')

</main>

<footer style="background: rgba(0,0,0,0.69)">

    <div class="container-fluid">
        <div class="row" style="padding-top: 50px; color: white;">

            <div class="col-lg-6 col-md-12 text-center" style="padding-top: 100px; padding-bottom: 50px;">
                <img src="{{ asset('assets/img/logo-white.png') }}" height="60cm" alt="logo-white">
            </div>

            <div class="row">

                <div style="margin-left: 30px;">
                    <h5>A SAVOIR:</h5>
                    <nav class="nav flex-column footer-links">
                        <a class=" nav-link " href="#">QU'EST-CE QUE LE COVID-19?</a><br>
                        <a class=" nav-link " href="#">COMMENT SE PROTEGER?</a><br>
                        <a class=" nav-link " href="#">A QUOI SERVENT VOS INFORMATIONS?</a><br>
                        <a class=" nav-link " href="#">LES ACTUALITES SUR LE COVID-19</a><br>
                    </nav>
                </div>

                <div style="margin-left: 30px;">
                    <h5>MENU</h5>
                    <nav class="nav flex-column footer-links">
                        <a class=" nav-link " href="#">ACCEDER A VOTRE ESPACE</a><br>
                        <a class=" nav-link " href="#">S'ENREGISTRER</a><br>
                        <a class=" nav-link " href="#">DECONNEXION</a><br>
                        <a class=" nav-link " href="#">RECHERCHER</a><br>
                        <a class=" nav-link " href="#">DENONCER UN CAS</a><br>
                    </nav>
                </div>

            </div>

        </div>

    </div>
    <div class="text-center" style="color: white; padding-bottom: 10px;">
        <b>© 2020 DEVVIE.</b>
        <br>
        <span>Tous droits réservés.</span>
    </div>
</footer>

<!-- JAVASCRIPT -->

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>



<!-------------------------->
</body>
</html>
