@extends('template.frontend')

@section('title','Accueil')

@section('content')

    <section id="entete" style="padding-top: 40px; background-image: url('{{ asset('assets/img/background.png') }}'); background-repeat: no-repeat; background-size: cover; ba">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-6" style="margin-bottom: 10px">
                    <img src="{{asset("assets/img/logo.png")}}" height="100em" alt="logo-covid-stop">
                </div>
                <div class="col">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link" href="#">ACCUEIL</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">RECHERCHER</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">S'ENREGISTRER</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">ACCEDER A VOTRE ESPACE</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div style="margin-top: 100px;">
                <div class="row">
                    <div class="col-lg-5" style="color: white;">
                        <h3>A QUOI SERT CETTE <br> PLATEFORME?</h3>
                        <p style="margin-top: 50px">Dans le but de réduire les risques de propagation du Virus, nous avions mis en place cette plateforme permettant de recenser les cas suspects et d'enregistrer leurs trajets.</p>
                        <p>Ces cas seront automatiquement pris en charge. De même que ceux qui ont été en contact avec eux.</p>
                        <a class="btn btn-lg" href="" style="background-color: #1abc9c; color: white; margin-top: 50px;">S'ENREGISTRER</a>
                        <a class="btn btn-lg" href="" style="background-color: #1abc9c; color: white; margin-top: 50px;">VOUS ETES DEJA ENREGISTRES</a>
                    </div>
                </div>
            </div>

            <div class="text-center" style="padding: 50px;">
                <a href="#que-faire" class="suivant"><img src="{{asset('assets/img/icons/down-green.png')}}" alt=""></a>
            </div>
        </div>
    </section>

    <section id="que-faire" style="padding-top: 100px; padding-bottom: 100px;">


        <h1 class="text-center">QUE FAIRE ? </h1>


        <div class="container">
            <div class="row" style="margin-top: 100px;">
                <div class="col">
                    <div class="card" style="height: 20rem">
                        <div class="card-body">
                            <h5 class="card-title text-center">S'ENREGISTRER</h5>
                            <p class="card-text" style="margin-top: 30px;">
                                L'enrégistrement est réservé aux personnes qui ont été de retour d'un voyage ce mois ou/et qui ont des symptomes
                            </p>

                        </div>
                        <div class="card-footer text-center">
                            <a href="#" class="btn btn-primary text-right">Continuer > </a>
                        </div>
                    </div>
                </div>

                <div class="col">
                    <div class="card" style="height: 20rem">
                        <div class="card-body">
                            <h5 class="card-title text-center">RECHERCHER</h5>
                            <p class="card-text" style="margin-top: 30px;">
                                Effectuer une recherche sur les lieux à risque et sur les personnes suspectes
                            </p>

                        </div>
                        <div class="card-footer text-center">
                            <a href="#" class="btn btn-primary text-right">Continuer > </a>
                        </div>
                    </div>
                </div>

                <div class="col">
                    <div class="card" style="height: 20rem">
                        <div class="card-body">
                            <h5 class="card-title text-center">DENONCER UN CAS</h5>
                            <p class="card-text" style="margin-top: 30px;">
                                Si vous connaissez une personne qui est rentrée de voyage ou qui présente des symptomes, veullez nous contacter ou juste l'enrégistrer.
                            </p>

                        </div>
                        <div class="card-footer text-center">
                            <a href="#" class="btn btn-primary">Continuer > </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="text-center" style="padding-top: 100px;">
            <a href="#denoncer-un-cas" class="suivant"><img src="{{asset('assets/img/icons/down-orange.png')}}" alt=""></a>
        </div>

    </section>

    <section id="denoncer-un-cas" style="background-image: url('{{ asset('assets/img/divider.png') }}');">
        <div class="text-center" style="padding: 50px;">
            <h1>ENSEMBLE, <br> LUTTONS CONTRE LE COVID-19</h1>
            <a href="" class="btn btn-lg" style="margin-top: 50px; background: #e74c3c;  color: white;">DENONCER UN CAS</a>
        </div>

    </section>

@endsection()
