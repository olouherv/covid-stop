@extends('template.frontend')

@section('title','Connexion')

@section('content')

    <div class="text-center" style="padding: 50px">
        <a href="/">
            <img src="{{ asset('assets/img/logo.png') }}" alt="Accueil" height="80px">
        </a>
    </div>

    <div class="container text-center" style="padding-bottom: 20px;">
        <div class="row">
            <div class="col-md-3 col-lg-4"></div>
            <div class="col-md-6 col-lg-4 formulaire">
                <form action="" id="formulaire-connexion">
                    <div class="form-group">
                        <label for="login">Login*</label>
                        <input type="text" class="form-control" name="login" autocomplete="off" placeholder="Login">
                    </div>

                    <div class="form-group">
                        <label for="mot_de_passe">Mot de passe*</label>
                        <input type="password" class="form-control" name="mot_de_passe" autocomplete="off" placeholder="Mot de passe">
                    </div>

                    <input type="submit" class="btn my_btn_success" value="Connexion" >

                </form>
            </div>
            <div class="col-md-3 col-lg-4"></div>
        </div>
    </div>

@endsection()
