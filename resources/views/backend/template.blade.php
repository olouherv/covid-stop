<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">


    <!-- CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">

    <!-------------------------->
    <title>@yield('title')</title>
</head>
<body>

<header>
    <nav class="navbar" style="background: #1abc9c">
        <div class="container" style="color: white;">
            <div style="font-size: smaller"> Numéro d'urgence : +229 XX XX XX XX</div>
            <div style="font-size: smaller"> Suivez nous : <a href=""><img src="{{ asset('assets/img/icons/twitter.png') }}"></a></div>
        </div>

    </nav>
</header>

<main>

    <section id="entete" style="padding-top: 40px; background-image: url('{{ asset('assets/img/background.png') }}'); background-repeat: no-repeat; background-size: cover; ba">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-6" style="margin-bottom: 10px">
                    <img src="{{asset("assets/img/logo.png")}}" height="100em" alt="logo-covid-stop">
                </div>
                <div class="col">
                    <ul class="nav">
                        <li class="nav-item">
                            <a class="nav-link" href="#">ACCUEIL</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">RECHERCHER</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">S'ENREGISTRER</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">ACCEDER A VOTRE ESPACE</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div style="margin-top: 100px;">
                <div class="row">
                    <div class="col-lg-5" style="color: white;">
                        <h3>A QUOI SERT CETTE <br> PLATEFORME?</h3>
                        <p style="margin-top: 50px">Dans le but de réduire les risques de propagation du Virus, nous avions mis en place cette plateforme permettant de recenser les cas suspects et d'enregistrer leurs trajets.</p>
                        <p>Ces cas seront automatiquement pris en charge. De même que ceux qui ont été en contact avec eux.</p>
                        <a class="btn btn-lg" href="" style="background-color: #1abc9c; color: white; margin-top: 50px;">S'ENREGISTRER</a>
                    </div>
                </div>
            </div>

            <div class="text-center" style="padding: 50px;">
                <a href="#que-faire"><img src="{{asset('assets/img/icons/down-green.png')}}" alt=""></a>
            </div>
        </div>
    </section>

    <section id="que-faire" style="padding-top: 100px; padding-bottom: 100px;">


        <h1 class="text-center">QUE FAIRE ? </h1>


        <div class="container">
            <div class="row" style="margin-top: 100px;">
                <div class="col">
                    <div class="card" style="height: 20rem">
                        <div class="card-body">
                            <h5 class="card-title text-center">S'ENREGISTRER</h5>
                            <p class="card-text" style="margin-top: 30px;">
                                L'enrégistrement est réservé aux personnes qui ont été de retour d'un voyage ce mois ou/et qui ont des symptomes
                            </p>

                        </div>
                        <div class="card-footer text-center">
                            <a href="#" class="btn btn-primary text-right">Continuer > </a>
                        </div>
                    </div>
                </div>

                <div class="col">
                    <div class="card" style="height: 20rem">
                        <div class="card-body">
                            <h5 class="card-title text-center">RECHERCHER</h5>
                            <p class="card-text" style="margin-top: 30px;">
                                Effectuer une recherche sur les lieux à risque et sur les personnes suspectes
                            </p>

                        </div>
                        <div class="card-footer text-center">
                            <a href="#" class="btn btn-primary text-right">Continuer > </a>
                        </div>
                    </div>
                </div>

                <div class="col">
                    <div class="card" style="height: 20rem">
                        <div class="card-body">
                            <h5 class="card-title text-center">DENONCER UN CAS</h5>
                            <p class="card-text" style="margin-top: 30px;">
                                Si vous connaissez une personne qui est rentrée de voyage ou qui présente des symptomes, veullez nous contacter ou juste l'enrégistrer.
                            </p>

                        </div>
                        <div class="card-footer text-center">
                            <a href="#" class="btn btn-primary">Continuer > </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="text-center" style="padding-top: 100px;">
            <a href="#denoncer-un-cas"><img src="{{asset('assets/img/icons/down-orange.png')}}" alt=""></a>
        </div>

    </section>

    <section id="denoncer-un-cas" style="background-image: url('{{ asset('assets/img/divider.png') }}');">
        <div class="text-center" style="padding: 50px;">
            <h1>ENSEMBLE, <br> LUTTONS CONTRE LE COVID-19</h1>
            <a href="" class="btn btn-lg" style="margin-top: 50px; background: #e74c3c;  color: white;">DENONCER UN CAS</a>
        </div>

    </section>

</main>

<footer style="background: rgba(0,0,0,0.69)">

    <div class="container-fluid">
        <div class="row" style="padding-top: 50px; color: white;">

            <div class="col-lg-6 col-md-12 text-center" style="padding-top: 100px; padding-bottom: 50px;">
                <img src="{{ asset('assets/img/logo-white.png') }}" height="60cm" alt="logo-white">
            </div>

            <div class="row">

                <div style="margin-left: 30px;">
                    <h5>A SAVOIR:</h5>
                    <nav class="nav flex-column footer-links">
                        <a class=" nav-link " href="#">QU'EST-CE QUE LE COVID-19?</a><br>
                        <a class=" nav-link " href="#">COMMENT SE PROTEGER?</a><br>
                        <a class=" nav-link " href="#">A QUOI SERVENT VOS INFORMATIONS?</a><br>
                        <a class=" nav-link " href="#">LES ACTUALITES SUR LE COVID-19</a><br>
                    </nav>
                </div>

                <div style="margin-left: 30px;">
                    <h5>MENU</h5>
                    <nav class="nav flex-column footer-links">
                        <a class=" nav-link " href="#">ACCEDER A VOTRE ESPACE</a><br>
                        <a class=" nav-link " href="#">S'ENREGISTRER</a><br>
                        <a class=" nav-link " href="#">RECHERCHER</a><br>
                        <a class=" nav-link " href="#">DENONCER UN CAS</a><br>
                    </nav>
                </div>

            </div>

        </div>

    </div>
    <div class="text-center" style="color: white; padding-bottom: 10px;">
        <b>© 2020 DEVVIE.</b>
        <br>
        <span>Tous droits réservés.</span>
    </div>
</footer>

<!-- JAVASCRIPT -->

<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>



<!-------------------------->
</body>
</html>
