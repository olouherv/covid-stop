@extends('template.backend')

@section('title','Espace personnel')

@section('content')
    <div class="container">
        <div class="row" style="margin-top: 80px;">
            <div class="col-6"></div>
            <div class="col-lg-6 col-sm-12 text-center my-auto" style="font-size: large;">
                Vous êtes dans votre espace personnel. <br>
                Dans le menu à gauche, vous avez le bouton qui vous permet <br>
                d'accéder à la page d'enregistrement de vos déplacement.<br>
                Cela nous aidera à retracer les différents lieux par où nous commencerons les analyses.<br>
            </div>
        </div>
    </div>
<div class="text-center" style="padding: 50px;">



</div>

@endsection()
